#ifndef _INCLUDE_BWT_BWT_H
#define _INCLUDE_BWT_BWT_H

#include<string>
#include<algorithm>
#include<vector>

class BWT{
    public:
    static std::string encode(std::string input_string);
    static std::vector<std::string> rotations(std::string input_string);
};

#endif