#include "include/bwt/BWT.h"

std::string BWT::encode(std::string input_string){
    input_string.append("$");

    std::vector<std::string> rotations;

    //TODO: use suffix array instead of this
    int n = input_string.length();
    for(int i=0; i<n; i++){
        std::string rotation;
        for(int j=i; j<n; j++)
            rotation += input_string[j];
        for(int j=0; j<i; j++)
            rotation += input_string[j];
        rotations.push_back(rotation);
    }

    std::sort(rotations.begin(), rotations.end());

    std::string output_string;
    for(int i=0; i<n; i++)
        output_string += rotations[i][n-1];
    
    return output_string;
};

std::vector<std::string> BWT::rotations(std::string input_string){
    input_string.append("$");

    std::vector<std::string> rotations;

    //TODO: use suffix array instead of this
    int n = input_string.length();
    for(int i=0; i<n; i++){
        std::string rotation;
        for(int j=i; j<n; j++)
            rotation += input_string[j];
        for(int j=0; j<i; j++)
            rotation += input_string[j];
        rotations.push_back(rotation);
    }

    std::sort(rotations.begin(), rotations.end());

    return rotations;
};