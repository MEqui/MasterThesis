#include<iostream>
#include<string>

#include "include/bwt/BWT.h"

int main(int argc, char const *argv[])
{
    if(argc < 2) return 0;


    if(argc > 2){
        std::string option = argv[1];
        std::string input_string = argv[2];
        if(!option.compare("-r")){
            std::vector<std::string> rotations = BWT::rotations(input_string);
            for(int i=0; i<rotations.size(); i++)
                std::cout << rotations[i] << "\n";
        }
    }
    else{
        std::string input_string = argv[1];
        std::cout << BWT::encode(input_string) << "\n";
    }

    return 0;
}
