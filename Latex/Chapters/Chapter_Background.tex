\lhead{\emph{\ref{chapter:background} Historical  Background}}
\chapter{Historical Background} \label{chapter:background}
\emph{PMLG} may be considered a logical evolution of the pattern matching in plain text problem, i.e. find where a string occurs within another. When \emph{PMLG} was first proposed the techniques used for the plain text case could not be easily generalized to be effective for working on a graph. Due to this, original approaches had to be followed in order to discover suitable solutions. In this chapter, after a brief historical summary of the pattern matching in plain text problem problem, we describe which solutions had been proposed over the years for \emph{PMLG}, how algorithms improved and finally why we have chosen to provide a lower bound for this problem.

\section{Before \emph{PMLG}: Pattern Matching in Plain Text}

% Parent problem: pattern matching in plain text
The problem of finding a string within another one (or a pattern inside a plain text) has been studied since the 1970s and several solutions have been proposed both for finding an exact and an approximate match. In this section we briefly summarize the history of this problem and how this relates to our work.

\subsection{Exact Matching}
A na\"ive algorithm would try to align the first string to all of the positions of the second string. Such brute-force approach clearly requires a quadratic number of comparisons and hence a $O(nm)$ time complexity, where $n$ and $m$ are the length of the first and second string, respectively. Already in 1973 a major improvement was achieved by the KMP (Knuth-Morris-Pratt) algorithm \cite{knuth1973art, knuth1977fast}. This algorithm shows how a preprocessing step on the shorter string allows to build a data structure implementing a so called \emph{failure} function. When trying to align one string to the other, such data structure determines in constant time which is the longest shift we can afford without losing valid solutions. in this way the time complexity was lowered to $O(n + m)$ (preprocessing time included).\\
In 1975 the Aho-Corasick algorithm \cite{aho1975efficient} was proposed. Based on the same principle of KMP, this algorithm allowed to match multiple patterns against the same text at the same time. A failure function similar to the KMP one provided a way to efficiently jump among the sates of an automaton for an overall time complexity of $O(n + m')$, where $m'$ is the total length of all the patterns.\\
Several other approaches have been adopted for solving the pattern matching problem but going through all of them falls outside the goals of this thesis. Nevertheless, we mention Horspool's algorithm \cite{horspool1980practical} as an example of a different strategy. This algorithm scans the pattern backward, trying to find the first character that matches the current text character. To speedup this process, a look-up table is used. Such table has size $O(\sigma)$, where $\sigma$ is the cardinality of the alphabet in use. Horspool's algorithm has a $O(nm)$ worst case time complexity but it reaches $O(n/\min \{m, \sigma\})$ time on the average, meaning high performances when the alphabet is large.\\
Finally, some algorithms reached a linear time complexity $O(n)$. Crochermore's algorithm \cite{crochemore1992string} is was one of them. We omit the details since they are not trivial, but the main idea was to improve the KMP approach avoiding to entirely preprocess the pattern.\\

\subsection{Approximate Matching}
All of the algorithms described so far deal with the problem of finding an exact match of a string within another. An harder problem w.r.t. time complexity is the one of determining an approximate match between two strings. For solving such a problem first we have to specify what we mean by ``approximate''. In order to do so, a concept of \emph{distance} between strings have to be introduced. A well known distance is the classical \emph{edit distance} - also called Levenshtein distance - which consist in counting how many edit operations (insertion, deletion and substitution of a character) are needed to convert one string into another. Given such definition of distance between two strings, we can now define approximate pattern matching as follows. Given a pattern and a text, find all positions in the text at which the pattern has minimum distance match with a substring ending at that position. The classic solution for this problem is a dynamic programming algorithm which runs in $O(nm)$ time \cite{levenshtein1966bcc, navarro2001guided}.

\subsection{From Plain Text to Graphs}
The problem of finding the minimal distance occurrence for a pattern in a text heavily relates with an approximate formulation of \emph{PMLG}. Indeed, in \cite{rautiainen2017aligning} it has been shown that the approximate pattern matching in plain text problem is a special case of the approximate pattern matching in labeled graph problem and that the algorithm proposed for the latter works for the former as well. The time complexity is $O(|V| + |E|m)$, where $|V|$ and $|E|$ are respectively the number of nodes and edges of a labeled graph that has only one character per node. When the input consists of just two strings, such algorithm runs in the same time as the classic dynamic programming algorithm for edit distance, i.e. $O(nm)$. In 2015 it was proven that $O(nm)$ is a lower bound for the time needed to calculate the edit distance between two strings unless \emph{SETH} is false \cite{bringmann2015quadratic}. A straightforward consequence of this is that the algorithm proposed in \cite{rautiainen2017aligning} is optimal for the labeled graph case, unless \emph{SETH} is false. In fact, a faster algorithm for the labeled graph problem would be a faster algorithm also for computing standard edit distance.

\section{First Algorithms}
This sections describes how the studies about \emph{PMLG} evolved from the 1990s until year 2000. During this period, slightly different versions of \emph{PMLG} were taken into account while exploring new approaches. In the 2000s this problem did not receive much attention due two its difficulties and the fact that better solutions for the same practical applications were provided by other fields of computer science. Indeed, in the 1990s \emph{PMLG} was commonly referred to as \emph{Pattern Matching in Hypertext} since one of the possible applications was finding a string following the links of web pages. In the next sections we will outline the history of \emph{PMLG} starting from its birth, thus we might use the expression \emph{hypertext} in place of \emph{labeled graph}, but in this context the two terms are absolutely equivalent.\\
For what regarding the recent years, as mentioned in Chapter \ref{chapter:intro} the increasing interest in the study of DNA strongly motivated a more accurate analysis of this problem, which is now playing a central role in bioinformatics.

\subsection{Pioneering Work}
The first studies regarding this problem started in the 1990s. \emph{PMLG} arose in 1992 when Manber and Wu pioneered the first algorithm \cite{manber1992approximate}. Prior to this work there had been studies about the problem of matching a string against a regular expression or a network, but these related to \emph{PMLG} only as a consequence. Manber and Wu where the first that focused specifically on pattern matching in text and hypertext. Indeed, they proposed an algorithm for performing \emph{approximate pattern matching} and showed that it was suitable both for text and hypertext. Given a text $T$ and a pattern $P$, solving an approximate pattern matching problem means to find all the matches for $P$ in $T$ of minimal error. In such environment, the error is defined as the distance - usually the edit distance - between two strings. When the underlining structure is a graph instead of a standard text, then it is referred to as hypertext. When dealing with an hypertext, the concept of match is defined in the same way as in \emph{PMLG} but for the fact that it is enough for the pattern to be at minimal distance (instead of being exactly the same string) to the string obtained by concatenating the labels of the nodes in a path. Manber and Wu took into account three situations: facing a plain text, a tree and a DAG. Their algorithm run in $O(|V| + occ \, \log\log m)$ time when handling a text or a tree while it requires $O(|V| + occ \, \log\log m + |E|m)$ time in the case of a DAG. Variable $occ$ stands for the number of occurrences of a valid match for the pattern. Although the case of a general graph was not covered yet, the iconic feature that will characterize our lower bound was already emerging from this work: the $|E|m$ term in the time complexity. As we will see in the following, $|E|m$ represented a serious difficulty to overcome and whenever an attempt was made to handle DAGs and general graphs it was not possible to lower this term.

\subsection{Further Developments}
After the first result by Manber and Wu, in the following years quite few improvements were achieved for some special cases of the problem. In 1993 Akutsu proposed an algorithm for exact pattern matching in a hypertext with a tree structure that was running in $O(N)$ time \cite{akutsu1993linear}. Here $N$ refers to the total length of all the strings in the hypertext, i.e. the total number of characters in the hypertext. After few years, in 1995, Park and Kim solved the problem of performing exact pattern matching in a DAG with a time complexity of $O(N +m|E|)$ \cite{park1995string}. However, their algorithm had a crucial caveat since they were requiring the matches to span only one edge. At the end of the 1990s these limitations were finally surpassed by Amir et al. \cite{amir1997pattern}. In this work they extensively analyzed both the exact and the approximate case also giving some essential limitations for the latter. They improved the previous results on exact matching with an algorithm able to work even on a general graph in $O(N + m|E|)$. Even if this result comes from the late 1990s it still is one of the best algorithms we have for solving \emph{PMLG} and we will extensively talk about this in the next section. They found a better solution also for the approximate case, proposing an algorithm able to run in $O(mN\log N + m|E|)$ time. Furthermore, Amir et al. also focused on the hardness of approximate pattern matching in labeled graphs. When computing the standard edit distance between two strings, what we are trying to find is the smallest amount of edit operations needed to convert the first string into the second one, or vice versa. Therefore, the problem is perfectly symmetrical and we can say that we are allowing to have errors in either of the two strings. If we are matching a pattern against an hypertext this becomes more complicated. Indeed, the cardinal issue is allowing errors in the hypertext. Since the hypertext is a graph, its structure could need a significant reshape if we are deleting or inserting a node. On the other hand, inserting or deleting a character in a string is quite simple, hence the problem is no longer symmetrical. What Amir et al. pointed out is that if we allow to have errors in the hypertext then the problem becomes NP-Complete. This result has major implications also in the practical applications. If we think about modeling a reference genome with a labeled graph we suddenly realize that we can not rely solely on the pattern matching algorithm for handling possible reading errors occurred while scanning the real DNA or eventual mutations. In fact, this is exactly the case in which we would like to allow errors in the graph, which inevitably leads to an NP-Complete problem.\\
We conclude the first phase of the history of \emph{PMLG} with Navarro's work \cite{navarro2000improved}. In this paper he presented a new algorithm for the approximate case which was running in $O(mN + m|E|)$ time. This result remained the best one available for years due to a loss of interest in studying the problem. Recently, in 2017, two papers focused again on \emph{PMLG} \cite{Vaddadi2017sequence, rautiainen2017aligning}. In particular, Rautiainen and Marschall \cite{rautiainen2017aligning} illustrated an improved algorithm for the approximate case with a time complexity of $O(|V| + m|E|)$. Thanks to its structure, such algorithm can be seen as the natural generalization to the labeled graph case of the dynamic programming algorithm used for computing standard edit distance between two strings. Since edit distance can not be computed in strongly subquadratic time unless \emph{SETH} is false \cite{Backurs2014edit}, they could easily show that $O(m|E|)$ is a conditional lower bound (based on \emph{SETH}) for the approximate pattern matching in labeled graph problem (approximate \emph{PMLG}) and that their algorithm matches such lower bound.\\
At this point in time, what was still missing was some kind of lower bound for the exact case, i.e. for \emph{PMLG} as we first defined it, and this is what we achieved in this thesis.

\section{Best Result for Exact Pattern Matching}
One of the results presented in \cite{amir1997pattern} is an algorithm for solving exact \emph{PMLG}. Such algorithm represents one of the best solutions proposed for the exact pattern matching case with a time complexity of $O(N + m|E|)$. Observe that Rautiainen and Marschall's algorithm runs in $O(|V| + m|E|)$ time and since $N \geq |V|$ (each node has at least one character), one could think to use such algorithm considering exact matching a special case of approximate matching eventually achieving better performances. However, it has to be emphasized that there were meaningful differences between the graphs considered. Amir et al. were working on an undirected graph with arbitrary strings in each node while Rautiainen and Marschall considered a directed graph that had one character per node. In this sense, Rautiainen and Marschall also reached a $O(N + m|E|)$ time complexity.\\
Although describing all the details goes beyond the goals of this thesis, we will go through the main passages that Amir et al. followed in their work, in order to understand from where the $O(m|E|)$ element arises. The first step consists in transforming the original graph $H$ in a new one called \emph{one-character hypertext} $\mathtt{OH}$. Every node holding a string of length $l$ is split into $l$ new nodes ``saving for each new vertex its origin'', as reported in \cite{amir1997pattern}. Given $\mathtt{OH}$, a new graph $H'$ is built. Consider that we are given a pattern $P = p_1 \ldots p_m$ of length $m$. For every one-character node $v$, $m$ new nodes $v_1, \ldots , v_m$ are created. An edge $(v_i, v'_{i+1})$ is placed if $p_i$ matches the character in $v_i$ and there was and edge from $v$ to $v'$ in $\mathtt{OH}$. Finally, nodes $s$ and $f$ are added along with edges $(s,v_1)$ for every $v_1$ and $(v_m,f)$ for each $v_m$ whose label matches $p_m$. Figure \ref{figure:amirlewensteintransf} shows and example of this construction.

\begin{figure}[h]
\centering
\includegraphics[scale=0.50]{Figures/AmirLewensteinTransformation.pdf}
\caption{An example of the construction of the final graph starting from the pattern and the one-character hypertext.}
\label{figure:amirlewensteintransf}
\end{figure}

At this point one can perform a DFS visit on the new graph $H'$ and mark every node $v_i$ which is able to reach node $f$ via a path of length exactly $m$. This last operation allows to report every node $v$ in $H$ corresponding to a marked node $v_i$ in $H'$ as the beginning of a match for pattern $P$. For what regarding the time complexity, the most demanding operation is the construction of $H'$. The time spent is clearly linear to its size. Consider the one-character hypertext $\mathtt{OH}$ and let $|E'|$ be the number of its edges. $|E'| = N + |E|$ since we have the staring edges plus one newly added edge for each character. When building $H'$ every node is expanded in $m$ new nodes and for each one of them an edge could be placed. Hence the final time complexity is $O(m|E'|) = O(m(N + |E|)) = O(mN + m|E|)$. In \cite{amir1997pattern} they present this algorithm alongside with and improved version of it which finally reaches $O(N + m|E|)$ time.\\
Following this it can be seen that the hard part in solving this problem is handling all the ways in which the pattern can match a node. In fact, given a node, any prefix and any suffix of the pattern could have a match in that node, and manage all these possibilities at the same time is very difficult. This is indeed the main feature that we exploited in the proof of our lower bound.

\section{Best Result for Approximate Pattern Matching}
In \cite{amir1997pattern} Amir et al. proposed algorithms for both exact and approximate matching. In the case of approximate matching they achieved $O(mN\log N + m|E|)$ time complexity. Few years later Navarro improved this result with a $O(mN + m|E|)$ algorithm that remained the best solution for years \cite{navarro2000improved}. As mentioned earlier, in 2017 Rautiainen and Marschall \cite{rautiainen2017aligning} studied again the problem of approximate \emph{PMLG} and reached $O(|V| + m|E|)$ time complexity. Additionally, they showed that it is hard to have better performances without contradicting \emph{SETH}. Remember that Rautiainen and Marschall worked on a directed graph having one character per node, which means they had a $O(N + m|E|)$ time complexity if compared to the results from Amir et al.\\
In this section we illustrate the main ideas behind the solution presented in \cite{rautiainen2017aligning}. First, recall how the standard edit distance is computed: a dynamic programming matrix is created and the costs in each cell are updated row by row or column by column. With some crucial modifications it is possible to make this approach work also for \emph{PMLG}. In order to match a pattern against a graph, a dynamic programming table similar to the one used in the edit distance case is built. In this table there is one row for each character in the pattern and one column for each node of the graph. Figure \ref{figure:DPtable} shows an example of this construction.

\begin{figure}[h]
\centering
\includegraphics[scale=0.40]{Figures/DPtable.pdf}
\caption{The DP matrix of the alignment between a sequence graph (top) and a sequence
(left). The dashed arrows are the backtrace and the solid arrows show the optimal
alignment (image and caption taken from \cite{rautiainen2017aligning}).}
\label{figure:DPtable}
\end{figure}

At this point, the hard part consists in updating the costs in the cells. To this aim, the following recurrence is defined:

\begin{align*}
    C_{i,j} = min
        \begin{cases}
            C_{k,j-1} + \Delta_{i,j} \qquad &\text{ for } k \in \delta_i^\text{in}\\
            C_{k,j} + 1 \qquad &\text{ for } k \in \delta_i^\text{in}\\
            C_{k,j-1} + 1\qquad \\
        \end{cases}
\end{align*}

where $\Delta_{i,j}$ is the mismatch penalty between the character of node $v_i$ and sequence character $s_j$ ($0$ for a match, $1$ for a mismatch) and $\delta_i^\text{in}$ is the set of indices of in-neighbours of $v_i$. first of all, they formally demonstrate that the costs assignment defined by such recursion always exists and is unique. However, the main issue to handle the second case of this recursion. In fact, trying to expand $C_{k,j}$ could be useless if there is a loop in the graph with $v_i$ and $v_k$ being part of it. Without going too much into the details, the way Rautiainen and Marschall solved the problem is defining the partial recursion:

\begin{align*}
    P_{i,j} = min
        \begin{cases}
            C_{k,j-1} + \Delta_{i,j} \qquad &\text{ for } k \in \delta_i^\text{in}\\
            C_{k,j-1} + 1\qquad \\
        \end{cases}
\end{align*}

and proving that $P_{i,j} - C_{i,j} \in {0,1}$. This is the key element on which their algorithm is based. Indeed, they show that, given this property, it is possible to scan the dynamic programming row by row and update it correctly. First, a row is visited and the ``almost'' correct costs are defined using the partial recursion. Second, such row is visited again and the costs are adjusted to the real one. During the second scanning the cells are visited in ascending order of cost for efficiency reasons. This description of the algorithm is meant to just give the idea since the formal details are many and non-trivial. What is relevant for us is the fact that they proved their algorithm runs in $O(|V| + m|E|)$ time.\\
When we analyzed \emph{PMLG} and how the algorithms for solving it improved through the course of the years we where surprised to discover that both the exact and the approximate case were solved with the same time complexity $O(N + m|E|)$ (as we explained earlier, $O(|V| + m|E|)$ is the same of $O(|V| + m|E|)$ since the assumption is to have one character per node). This fact is what inspired us the most and made us suppose that $O(m|E|)$ might have been a limitation no algorithm could ever overcome.

\section{State of the Art}
In this final section of this chapter we briefly summarize  what was the state of the art previous to this thesis. Table \ref{table:summary} highlights the key achievements that characterized the history of \emph{PMLG}.

\begin{table}[h]
\centering
\begin{tabular}{|c|c|c|c|c|}
     \hline
     \multicolumn{5}{|c|}{History of \emph{PMLG}}\\
     \hline\hline
     \textbf{Year} & \textbf{Authors} & \textbf{Graph} & \textbf{Exact/} & \textbf{Time}\\
     ~ & ~ & ~ & \textbf{Approximate} & ~\\
     \hline
     1992 & Manber, Wu & DAG & approximate$^\textit{(1)}$ & $O(|V| + m|E| + occ\log\log m)$\\
     \hline
     1993 & Akutsu & Tree & exact & $O(N)$\\
    \hline
    1995 & Park, Kim & DAG & exact$^\textit{(3)}$ & $O(N + m|E|)$\\
    \hline
    \rowcolor{lightgray}
    1997 & Amir et al. & general & exact & $O(N + m|E|)$\\
    \hline
    1997 & Amir et al. & general & approximate$^\textit{(1)}$ & $O(Nm\log N + m|E|)$\\
    \hline
    1997 & Amir et al. & general & approximate$^\textit{(2)}$ & NP-Hard\\
    \hline
    1998 & Navarro & general & approximate$^\textit{(1)}$ & $O(Nm + m|E|)$\\
    \hline
    2017 & Vadaddi, Sivadasan, & general & approximate$^\textit{(1)}$ & $O((|V|+1)m|E|)$\\
    ~ & Tayal, Srinivasan & ~ & ~ & ~\\
    \hline
    \rowcolor{lightgray}
    2017 & Rautiainen, Marschall & general & approximate$^\textit{(1)}$ & $O(N + m|E|)$\\
    \hline
\end{tabular}
\caption{Legend: $V$ = set of nodes, $E$ = set of edges, $occ$ =  number of matches for the pattern in the graph, $m$ = pattern   length, $N$ = total length of text in all nodes, \textit{(1)} errors only in the pattern, \textit{(2)} errors in the graph, \textit{(3)} matches span only one edge. The two rows highlighted in gray report the state of the art for exact and approximate pattern matching.}
\label{table:summary}
\end{table}

% History of the problem from 1992 to 2000
% Overview of the different approaches (exact/approximate, online/indexed)
% The O(E*m) time complexity (same best result for exact and approximate search in general graphs)
