\lhead{\emph{\ref{chapter:proof} Conditional Lower Bound}}
\chapter{Conditional Lower Bound}\label{chapter:proof}
\section{Notation}
Consider a \emph{SAT} formula $F$ and a labeled graph $G = (V,E, L)$. We will use the following notation:
\begin{itemize}
\item $n$ = number of variables in $F$;
\item $C$ = set of clauses in $F$;
\item $k$ = number of clauses in $F$, i.e. $|C|$;
\item $P$ = pattern that will be searched in $G$;
\item $m$ = length of the pattern which will be searched in $G$,
i.e $|P|$;
\item $x \models c$ means that truth assignment $x$ makes clause $c$ true;
\item $E$ means $|E|$ when used in the asymptotic notation;
\item $\Tilde{O}$ is the asymptotic notation adopted when ignoring
    polynomial factors, e.g. $O(k \, 2^\frac n2) = \Tilde{O}(2^\frac n2)$.
\end{itemize}

Furthermore, we must ensure that $k$ is at most sub exponential in $\frac n2$, i.e. $k = o(2^\frac n2)$. In fact, $k$ is part of the input of a \emph{SAT} problem and without this assumption it could be the case that $k = O(2^\frac n2)$. In such a situation, a brute force algorithm would have a time complexity of $O(k 2^n) = O(k 2^\frac n2 2^\frac n2) = O(k^3)$ which is polynomial in the input size and there would be no need to find anything more efficient than that.

\section{Definitions and Statement of the Theorem}
We report here the formal definition of the problem our dissertation is based on.
\begin{definition}\label{definition:labeledgraph} \emph{(Labeled Graph)}\\
    Given an alphabet $\Sigma$ s.t. $|\Sigma| \ge 2$, a labeled graph $G$ is a triplet $(V,E,L)$ where $(V,E)$ is a directed or undirected graph and $L: V \rightarrow \Sigma^+$ is a function that defines which string is assigned to each node.
\end{definition}
\begin{definition} \emph{(Match)}\\
    Let $u_1, \ldots, u_j$ be a path in $(V,E)$ and $P$ be a pattern. $L(u)[l,*]$ and $L(u)[*,l']$ are the suffix of $L(u)$ starting at position $l$ and the prefix of $L(u)$ ending at position $l'$, respectively. We say that $u_1 \ldots, u_j$ is a match for $P$ in $G$ with offset $l$ if the concatenation of the strings $L(u_1)[l,*] \cdot L(u_2) \cdot \ldots \cdot L(u_{j-1}) \cdot L(u)[*,l']$ matches $P$, for some $l'$.
\end{definition}
The \emph{PMLG} problem is then defined as:\\
\textbf{INPUT:} a labeled graph $G = (V,E,L)$ and a pattern $P$.\\
\textbf{OUTPUT:} all the matches for $P$ in $G$.

For our purpose it would be enough to exploit a relaxed version of the problem, namely to be able to determine whether or not there exist at least one match for $P$ in $G$, without reporting all of them.\\
We now define what is called \emph{SETH}: the \emph{Strong Exponential Time Hypothesis}. This is a statement which is believed to be true among the scientific community even though no formal prove has been provided yet.\\
\begin{definition} \emph{(SETH)}\\
    Let \emph{qSAT} be an instance of \emph{SAT} with at most \emph{q} literals per clause. Given
    \begin{equation*}
    \delta_q = inf \, \{\alpha \, : \, \text{There is an} \, O(2^{\alpha n}) \, \text{ time algorithm for qSAT}) \}
    \end{equation*}
    we state the following
    \begin{equation*}
    \text{\textbf{SETH:}} \qquad \lim\limits_{q \to \infty} \delta_q = 1
    \end{equation*}
\end{definition}

This hardness hypothesis was first proposed by Impagliazzo and Paturi \cite{impagliazzo1999complexity} and has been used several times for proving conditional lower bounds as explained by Vassilevska W. \cite{vassilevska2018fine-grained}. Given that the best known algorithm for \emph{PMLG} has an $O(Em)$ time complexity our goal is to prove the following

\begin{theorem}\label{theorem:Emlowerbound}
For any $\epsilon > 0$ and any alphabet $\Sigma$ such that $|\Sigma| \ge 4$, the \emph{Pattern Matching in Labeled Graphs} problem can not be solved in either $O(E^{1-\epsilon} \, m)$ or $O(E \, m^{1-\epsilon})$ time unless \emph{SETH} is false.
\end{theorem}

In order to achieve this goal we break down our reasoning process in
some intermediate steps. Since this is a conditional lower bound we
are trying to relate \emph{PMLG} to \emph{SAT}, therefore the first
thing we need is a reduction. Then we will show that having a too fast
algorithm for the pattern matching case would cause to solve
\emph{SAT} in $O(2^{cn})$ time with $c < 1$. This means that we can not afford to have a reduction which costs $O(2^n)$ time since it would not lead to such an algorithm. We need the reduction to cost $O(2^{cn})$ time with $c < 1$ as well. Hence the main steps can be synthesized as:
\begin{itemize}
\item find a reduction from \emph{SAT} to \emph{PMLG};
\item ensure that this reduction costs $O(2^{cn})$ time, $c < 1$; in particular, we will have a $\Tilde{O}(2^{\frac{2-\epsilon}{2}n}), \; \epsilon > 0$ reduction;
\item show that having a $O(E^{1 - \epsilon} \, m)$ or a $O(E \, m^{1 - \epsilon})$ time algorithm for \emph{PMLG} gives a solution for \emph{SAT} that contradicts $SETH$.
\end{itemize}

\section{Reduction}
Consider an instance of \emph{SAT} in which the $n$ variables are $v_1, \ldots,  v_n$. We call \emph{truth assignment} a tuple $(b_1, \ldots,  b_n)$ where $b_i \in \{True, False\}$ is the truth value assigned to variable $v_i$. We create $X$ from $v_1, \ldots, v_{\frac{n}{2}}$, which is the set of all possible truth assignments for the first half of the variables, assuming without loss of generality that $n$ is even. In the same manner we obtain $Y$ from $v_{\frac{n}{2} + 1}, \ldots, v_n$. Formally:
\begin{align*}
X =& \; \{x_i \, | \; x_i = (b_1^{(i)}, \ldots,  b_\frac{n}{2}^{(i)}) \quad \text{is a possible truth assignment for} \quad v_1, \ldots,  v_{\frac{n}{2}} \}&\\
Y =& \; \{y_j \, | \; y_j = (b_{\frac{n}{2}+1}^{(j)}, \ldots,  b_n^{(j)})\quad \text{is a possible truth assignment for} \quad v_{\frac{n}{2}+1}, \ldots,  v_n \}&
\end{align*}
From these two sets of truth assignments we generate an instance of \emph{PMLG}. Notice that $|X| = |Y| = 2^\frac n2$, which is the key for having a subexponential reduction. Building the graph properly and searching for the right pattern will allow us to determine whether the \emph{SAT} formula is satisfiable or not. Indeed, we will combine two different gadgets to build the graph. In the following we describe which pattern we will try to find a match for and how to define the nodes and the edges of the two gadgets.

\subsection{Pattern}
First of all we define the pattern we will look for in the graph. In order to do so we use the alphabet $\Sigma = \{ \texttt{b},\texttt{e},\texttt{c},\texttt{d} \}$. Consider set $X$ which contains all possible truth assignments for $v_1, \ldots, v_{\frac{n}{2}}$. The pattern $P = \texttt{b}P_{x_1}\texttt{eb}P_{x_2}\texttt{e} \ldots \texttt{b}P_{x_{2^{\frac{n}{2}}}}\texttt{e}$ for which we will try to find a match is the result of the concatenation of $2^{\frac{n}{2}}$ strings, $P_{x_i}$, each one of them corresponding to a different $x_i \in X$. The $h$-th character of string $P_{x_i}$ is defined as follows:
\begin{equation*}
P_{x_i}[h] =
    \begin{cases}
        \texttt{c} \qquad  \text{if} \quad x_i \not \models c_h\\
        \texttt{d} \qquad  \text{otherwise} 
    \end{cases}
\end{equation*}
Later we will prove that a \emph{SAT} formula is satisfiable if and only if we can find a match for this pattern in our graph.

\subsection{Gadget for Checking the \emph{SAT} Formula}
This gadget is an undirected graph $G_F = (V_F,E_F,L_F)$. Intuitively, we have a distinct node $c_{j h}$ for every possible pair $(y_j,c_h)$ such that  $y_j \in Y$ is an assignment that satisfies clause $c_h \in C$. Moreover, we will add a set of dummy nodes $d_{j h}$ for every possible pair $(y_j,c_h)$. After adding a \emph{begin} node $b$ and an \emph{end} node $e$, we will link $b$ to every $c_{h1}$ and $d_{h1}$, and every $c_{h k}$ and $d_{h k}$ to $e$. We also place an edge for every pair $(c_{j h}, c_{j h+1})$, $(c_{j h}, d_{j h+1})$, $(d_{j h}, c_{j h+1})$ and $(d_{j h}, d_{j h+1})$ of nodes that share the same $j$ and are consecutive in terms of $h$ coordinate. The labels are assigned from $\Sigma$ as one would expect: nodes $c_{j h}$ have label \verb|c|, nodes $d_{j h}$ have label \verb|d|, nodes $b$ and $e$ have labels \texttt{b} and \texttt{e}, respectively. We present now the formal details of what we just described. \figurename~\ref{figure:G_Fgadget} shows an example of $G_F$.

\begin{figure}[h]
\centering
\includegraphics[scale=0.40]{Figures/G_Fgadget_Alternative.pdf}
\caption{In this example we built gadget $G_F$ from formula $F$, where $n = 4, 2^\frac n2 = 4$ and $k = 3$. If for instance we look at the first column we observe that $c_{1 1}$ and $c_{3 1}$ are missing meaning that $y_1 \not\models c_1$ and $y_3 \not\models c_1$. On the other hand we know that $y_2 \models c_1$ and $y_4 \models c_1$ since we have nodes $c_{2 1}$ and $c_{4 1}$. An example of a pattern that we are able to match is $P = \texttt{b c c d e}$ while we would fail on $\bar{P} = \text{b c d c e}$.}
\label{figure:G_Fgadget}
\end{figure}

\paragraph{Nodes.}
Given clauses $c_1, \ldots , c_k \in C$ and set $Y$ of truth assignments we define:
\begin{align*}
V_F = \; &\{ c_{j h} \, | \, y_j \models c_h, \; y_j \in Y, c_h \in C \, \} \; \cup \\
&\{ d_{j h} \, | \,  y_j \in Y, c_h \in C \, \} \cup \{ b, e\}
\end{align*}

\paragraph{Edges.}
\begin{align*}
E_F = &\{ \left(b, c_{j 1}\right) \, | \, c_{j 1} \in V \} \cup \{ \left(b, d_{j 1}\right) \, | \, d_{j 1} \in V \} \, \cup \\
&\{ \left(c_{j k}, e\right) \, | \, c_{j k } \in V \} \cup \{ \left(d_{j k}, e\right) \, | \, d_{j k} \in V \} \, \cup \\
&\{ \left(c_{j h}, c_{j h+1}\right) \, | \, c_{j h}, c_{j h+1} \in V \} \, \cup \\
&\{ \left(c_{j h}, d_{j h+1}\right) \, | \, c_{j h}, d_{j h+1} \in V \} \, \cup \\
&\{ \left(d_{j h}, c_{j h+1}\right) \, | \, d_{j h}, c_{j h+1} \in V \} \, \cup \\
&\{ \left(d_{j h}, d_{j h+1}\right) \, | \, d_{j h}, d_{j h+1} \in V\}
\end{align*}

\paragraph{Labels.}
Let $u \in V$, we define $L_F: V \rightarrow \{\verb|b, e, c, d|\}$ as:
\begin{equation*}
L_F(u) = 
    \begin{cases}
        \texttt{b} \qquad \text{if} \quad u = b\\
        \texttt{e} \qquad \text{if} \quad u = e\\
        \texttt{c} \qquad \text{if} \quad u = c_{j h}\\
        \texttt{d} \qquad \text{if} \quad u = d_{j h}\\
    \end{cases}
\end{equation*}

\subsection{Universal Gadget}
Another gadget is needed in order to have all the tools for building the final graph. This is again an undirected graph $G_U = G(V_U,E_U,L_U)$. The idea is to make $G_U$ able to match any pattern $P' = \texttt{b} \, a_1 \, a_2 \ldots a_k \, \texttt{e}$, where $a_h \in \{ \texttt{c,d} \}$. Indeed, $G_U$ has to be able to match up to $2^{\frac{n}{2}}-1$ concatenated instances of this kind of pattern, namely $P' = \texttt{b}P'_1\texttt{e} \, \texttt{b}P'_2\texttt{e} \ldots \texttt{b}P'_{2^{\frac{n}{2}}-1}\texttt{e}$. The final goal is to make $G_U$ able to match those $P_{x_i}$ that are such that $x_i$ does not satisfies the \emph{SAT} formula $F$. $G_F$ will account for those $P_{x_i}$ for which it holds that $\forall \, c \in C \,.\, x_i \models c$. What we define now are $2^{\frac{n}{2}}-1$ copies of a substructure that matches any $P'_i$. This substructure has a node $b_i$ followed by nodes $c_{i h}$, $d_{i h}$ and finally node $e_i$, with $i=1, \ldots, 2^{\frac{n}{2}}-1, \; h=1, \ldots ,k$. For every $i$ and every $h$, the labels are $L_U(b_i) = \texttt{b}$, $L_U(c_{i h}) = \verb|c|$, $L_U(d_{i h}) = \verb|d|$ and $L_U(e_i) = \texttt{e}$. Think about nodes $c_{i h}$ and $d_{i h}$ as disposed in two parallel lines. We place the edges $(b_i, c_{i 1}), \, (b_i, d_{i 1}), \, (c_{i k}, e_j), \, (d_{i k}, e_i)$ for connecting the beginning and ending nodes of each substructure with its inner part. Furthermore we connect nodes $c_{i h}$ and $d_{i h}$ with the edges $(c_{i h}, c_{i h+1}), \, (c_{i h}, d_{i h+1}), \, (d_{i h}, c_{i h+1}), \, (d_{i h}, d_{i h+1})$. Finally we dispose our substructures one after the other thanks to the edges $(e_i, b_{i+1})$, for $i = 1, \ldots, 2^{\frac{n}{2}}-1$. A more formal definition is shown in the following paragraphs. In \figurename~\ref{figure:G_Ugadget} an example of the $G_U$ gadget is shown.

\begin{figure}[h]
\centering
\includegraphics[scale=0.70]{Figures/G_Ugadget.pdf}
\caption{In this example $n = 4, 2^\frac n2 = 4$ and $k = 3$. This graph can match any sequence of patterns $P{x_i}$ but its length is limited to $2^\frac n2 - 1$.}
\label{figure:G_Ugadget}
\end{figure}

\paragraph{Nodes.}
Given clauses $c_1, \ldots , c_k \in C$ we define:
\begin{align*}
V_U = \; &\{ c_{i h} \, | \, i,h \in \mathbb{N}, i \in \left[1,2^{\frac{n}{2}}-1\right], \; h \in \left[1,k\right] \} \; \cup \\
&\{ d_{i h} \, | \, i,h \in \mathbb{N}, i \in \left[1,2^{\frac{n}{2}}-1\right], \; h \in \left[1,k\right] \} \; \cup \\
&\{ b_i \, | \, i \in \mathbb{N}, i \in \left[1,2^{\frac{n}{2}}-1\right] \} \; \cup \{e_i \, | \, i \in \mathbb{N}, j \in \left[1,2^{\frac{n}{2}}-1\right]\}
\end{align*} 

\paragraph{Edges.}
\begin{align*}
E_U = &\{ \left(b_i, c_{i 1}\right) \, | \, b_i,c_{i 1} \in V \} \cup \{ \left(b_i, d_{i 1}\right) \, | \, b_i,d_{i 1} \in V \} \, \cup \\
&\{ \left(c_{i k}, e_i\right) \, | \, c_{i k }, e_i \in V \} \cup \{ \left(d_{i k}, e_i\right) \, | \, d_{i k}, e_i \in V \} \, \cup \\
&\{ \left(c_{i h}, c_{i h+1}\right) \, | \, c_{i h}, c_{i h+1} \in V \} \, \cup \\
&\{ \left(c_{i h}, d_{i h+1}\right) \, | \, c_{i h}, d_{i h+1} \in V \} \, \cup \\
&\{ \left(d_{i h}, c_{i h+1}\right) \, | \, d_{i h}, c_{i h+1} \in V \} \, \cup \\
&\{ \left(d_{i h}, d_{i h+1}\right) \, | \, d_{i h}, d_{i h+1} \in V \} \, \cup \\
&\{ \left(e_{i}, b_{i+1}\right) \, | \, e_{i}, b_{i+1} \in V \}
\end{align*}

\paragraph{Labels.}
Let $u \in V$, we define $L_U: V \rightarrow \{\verb|b, e, c, d|\}$ as:
\begin{equation*}
L_U(u) = 
    \begin{cases}
        \texttt{b} \qquad \text{if} \quad u = b_{i}\\
        \texttt{e} \qquad \text{if} \quad u = e_{i}\\
        \texttt{c} \qquad \text{if} \quad u = c_{i h}\\
        \texttt{d} \qquad \text{if} \quad u = d_{i h}\\
    \end{cases}
\end{equation*}

\subsection{Graph}
Now that we have both $G_F$ and $G_U$ we simply combine them to construct the actual graph that we will use to check the satisfiability of the \emph{SAT} formula. Take one instance of $G_F = (V_F, E_F, L_F)$ and two distinct but identical instances of $G_U$, say $G_U^{(1)} = (V_U^{(1)}, E_U^{(1)}, L_U^{(1)})$ and $G_U^{(2)} = (V_U^{(2)}, E_U^{(2)}, L_U^{(2)})$. The final graph $G = (V,E,L)$ has as nodes the union of all the nodes, hence $V = V_F \cup V_U^{(1)} \cup V_U^{(2)}$. Each node maintains the same label it had in the respective gadget, i.e $L = L_F \cup L_U^{(1)} \cup L_U^{(2)}$. The edges are the union of all the previous edges plus two. Indeed, we want to connect the \emph{``last''} \texttt{e} node of $G_U^{(1)}$ with the \texttt{b} node of $G_F$ and the \texttt{e} node of $G_F$ with the \emph{``first''} \texttt{b} node of $G_U^{(2)}$. Thus we have $E = E_F \cup E_U^{(1)} \cup E_U^{(2)} \cup \{(b_{2^\frac{n}{2}-1}^{(1)}, b),(e, e_1^{(2)})\}$, where $b_{2^\frac{n}{2}-1}^{(1)} \in V_U^{(1)}, \; b,e \in V_F, \; e_1^{(2)} \in V_U^{(2)}$. \figurename~\ref{figure:CompleteGraph} summarizes how we $G$ from $G_F$ and $G_U$.\\
Now that the construction of graph $G$ and pattern $P$ is completed, we claim that the \emph{SAT} formula we started from is satisfiable if and only if we can find a match for $P$ in $G$. In the next section we show how to prove this claim.

\begin{figure}[h]
\centering
\includegraphics[scale=0.40]{Figures/CompleteGraph_Alternative.pdf}
\caption{This figure shows how to construct $G$ in the example we proposed before, where $n = 4, 2^\frac n2 = 4$ and $k = 3$. What we need to introduce to obtain the final graph $G$ are the two solid edges that are connecting the two instances of $G_U$ with $G_F$ in the bottom part of the figure.}
\label{figure:CompleteGraph}
\end{figure}

\clearpage

\section{Correctness of the Reduction}
Having built pattern $P$ and graph $G$, we have to prove that the way in which we are checking the satifisfiability of the \emph{SAT} formula $F$ is correct, that is our reduction is correct. In order to do this we need to provide some lemmas. As a reminder, we recall that $P = \texttt{b}P_{x_1}\verb|eb|P_{x_2}\texttt{e} \ldots \texttt{b}P_{x_{2^{\frac{n}{2}}}}\texttt{e}$ and that $G$ is composed by two instances of $G_U$, say $G_U^{(1)}$ and $G_U^{(2)}$, and $G_F$. Now we are ready to move into the formal proofs.

\begin{lemma} \label{lemma:samej}
If a subpattern $P_{x_i}$ of pattern $P$ has a match in $G_F$ then all of the nodes of that match have the same $j$ coordinate.
\end{lemma}
\begin{proof}
Consider the path $\pi = b,u_1, \ldots, u_k,e$ as a match for $P_{x_i}$ in $G$. Let us suppose by contradiction that different nodes in $\pi$ can have different $j$ coordinate. This means that at a certain point there are two consecutive nodes $u_h$ and $u_{h+1}$ in $\pi$ with different $j$ coordinate. There can be four possible cases:
\begin{itemize}
    \item $b,u_1, \ldots, c_{jh}, c_{j'h+1}, \ldots, u_k, e$;
    \item $b,u_1, \ldots, c_{jh}, d_{j'h+1}, \ldots, u_k, e$;
    \item $b,u_1, \ldots, d_{jh}, c_{j'h+1}, \ldots, u_k, e$;
    \item $b,u_1, \ldots, d_{jh}, d_{j'h+1}, \ldots, u_k, e$;
\end{itemize}
where $j \neq j'$. The definition we gave for edges $E_F$ does not provide a way for supporting any of these cases. Indeed, there is no edge that allows to change the $j$ coordinate in the middle of a path. Hence we reached a contradiction.
\end{proof}

\begin{lemma} \label{lemma:matchinGF}
A subpattern $\texttt{b}P_{x_i}\texttt{e}$ of pattern $P$ has a match in $G_F \iff \exists \, j \, .$ assignment $(x_i,y_j) \models F$.
\end{lemma}
\begin{proof}
First of all, notice that the fact that we are using characters \texttt{b} and \texttt{e} in the pattern prevents from looping inside $G_F$, i.e. going back and forth between two or more $c$ and $d$ nodes. Since \texttt{b} and \texttt{e} always match nodes $b$ and $e$ in $G_F$, from now on we will focus only on $P_{x_i}$.\\
We handle the two implications individually.
\begin{itemize}
    \item [$\Rightarrow$] Consider partial assignment $x_i$. From the structure of the pattern we know that this assignment statisfies all the $c_h$ clauses for which $P_{x_i}[h] = \texttt{d}$. Since $P_{x_i}$ has a match in $G_F$, Lemma \ref{lemma:samej} ensures that there exist a $j$ such that all the nodes in such a match share that same $j$ coordinate. This means that assignment $y_j$ accounts for those clauses that $x_i$ can not satisfy, namely those ones that are such that $P_{x_i}[h] = \texttt{c}$. Hence we have found a complete assignment $(x_i ,y_j)$ which satisfies $F$.
    
    \item[$\Leftarrow$] We know that assignment $(x_i,y_j)$ makes all the clauses true. Specifically, we can say that $y_j$ satisfies at least all the clauses that $x_i$ does not satisfy. This implies that we can find a match for $P_{x_i}$ in $G_F$. In fact, if $P_{x_i}[h] = \texttt{d}$ then we know that $G_F$ has a node $d_{j h}$ that can match that character. If $P_{x_i}[h] = \texttt{c}$ then $x_i \not\models c_h$ and we need node $c_{jh}$ to exist. The existence of such a node is guaranteed from the fact that $y_j$ satisfies all the clauses that $x_i$ can not satisfy. In particular $y_j \models c_h$, hence we are sure we provided $G_F$ with node $c_{j h}$ during its construction. The definition of the edges of $G_F$ ensures that all the $c_{j h}$ and $d_{j h}$ nodes that we need are properly linked to allow us to find the match, and this is because they all share the same $j$ coordinate. We conclude that we have what we need for finding a match for $P_{x_i}$ in $G_F$.
    \end{itemize}
\end{proof}

\begin{lemma} \label{lemma:patternsubpattern}
Pattern $P$ has a match in $G$ $\iff \exists$ subpattern $\texttt{b}P_{x_i}\texttt{e}$ of $P$ \,.\, $\texttt{b}P_{x_i}\texttt{e}$ has a match in $G_F$.
\end{lemma}
\begin{proof}
For the $\Rightarrow$ implication, remember that $G_U^{(1)}$ and $G_U^{(2)}$ can match at most $2^\frac n2 -1$ subpatterns of $P$ each, while $P$ has $2^\frac n2$ of them. For this reason and because of the structure of $G$, at least one subpattern $\texttt{b}P_{x_i}\texttt{e}$ is forced to have a match in $G_F$ in order to have a full match for $P$.\\
On the other hand, the $\Leftarrow$ implication is trivially true. In fact, if $\texttt{b}P_{x_i}\texttt{e}$ has a match in $G_F$ then we can match $\texttt{b}P_{x_1}\texttt{e}, \ldots, \texttt{b}P_{x_{i-1}}\texttt{e}$ in $G_U^{(1)}$  and $\texttt{b}P_{x_{i+1}}\texttt{e}, \ldots, \texttt{b}P_{x_{2^{\frac{n}{2}}}}\texttt{e}$ in $G_U^{(2)}$ and have a full match for $P$ in $G$.
\end{proof}

The main purpose of Lemma \ref{lemma:samej} was to support Lemma \ref{lemma:matchinGF}. Now we show how to combine Lemmas \ref{lemma:matchinGF} and \ref{lemma:patternsubpattern} into one final lemma which proves the correctness of our reduction.

\begin{lemma} \label{lemma:correctness}
There exists a match for $P$ in $G$ $\iff \, F$ can be satisfied.
\end{lemma}
\begin{proof}
\begin{align*}
    &P \text{ has a match in } G&\\
    &\iff& \quad \{ \text{Lemma \ref{lemma:patternsubpattern}} \}\\
    &\exists \text{ subpattern } \texttt{b}P_{x_i}\texttt{e} \,.\, \texttt{b}P_{x_i}\texttt{e} \text{ has a match in } G_F&\\
    &\iff& \quad \{ \text{Lemma \ref{lemma:matchinGF}} \}\\
    &\exists j \,.\, (x_i,y_j) \models F&\\
    &\iff&\\
    &F \text{ can be satisfied}.
\end{align*}
\end{proof}

This lemma ensures that our reduction is correct. Before showing the contradiction with \emph{SETH} we still need to demonstrate that this reduction does not cost too much, i.e. it is subexponential. This is the issue handled in the next section.  

\section{Cost of the Reduction}
We analyze how much time we spend in building the graph and the pattern in terms of their size. Indeed, checking if an assignment satisfies a clause takes $O(n)$ time which, for our goals, is negligible when compared to $O(2^\frac n2)$. Additionally, remember that we are assuming $k = o(2^\frac n2)$.\\
Every $P_{x_i}$ in $P$ has $k$ characters that can be either \verb|c| or \verb|d| plus characters \texttt{b} and \texttt{e}. $P$ has $2^\frac n2$ sub-patterns $P_{x_i}$, hence summing everything up we get $(k+2) \, 2^\frac n2 = \Tilde{O}(2^\frac n2)$. $G_U$ has $2^\frac n2$ substructures each one having $k$ \verb|c| nodes, $k$ \verb|d| nodes and nodes $b_i$ and $e_i$, hence there are $(2 + 2k) \, 2^\frac n2 = \Tilde{O}(2^\frac n2)$ total nodes. Each node has a constant number of edges (at most $4$) thus their size is $\Tilde{O}(2^\frac n2)$ as well. For what regards $G_F$, it has $O(k \, 2^\frac n2)$ \verb|c| nodes and the same amount of \verb|d| nodes, plus $b$ and $e$. In this case, each node has a constant number of edges but for $b$ and $e$. Nevertheless, $b$ and $e$ have $O(2^\frac n2)$ edges each, therefore the total amount of edges is again $\Tilde{O}(2^\frac n2)$. For connecting $G_F$ to the two instances of $G_U$ we are adding just $2$ edges. Since the pattern and every part of the graph have a size of $\Tilde{O}(2^\frac n2)$, we conclude that the cost of our reduction is indeed $\Tilde{O}(2^\frac n2)$.

\section{Contradiction with \emph{SETH}}
The last step in our proof is to show that any $O(E^{1-\epsilon} \, m)$ or $O(E \, m^{1-\epsilon})$ algorithm for \emph{PMLG} unavoidably leads to a contradiction with \emph{SETH}. To this aim, assume that we have such an algorithm; we will refer to it as $A$. Given a \emph{SAT} formula $F$ we perform our reduction obtaining pattern $P$ and graph $G$ in $\Tilde{O}(2^\frac n2)$ time. We would like to use $A$ for determining whether or not there exists a match for $P$ in $G$. In this way we could use Lemma \ref{lemma:correctness} to determine if $F$ is satisfiable or not. Notice that due to the nature of our reduction we have $E = \Tilde{O}(2^\frac n2)$ and $m = \Tilde{O}(2^\frac n2)$. At this point, no matter if $A$ has $O(E^{1-\epsilon} m)$ or $O(E \, m^{1-\epsilon})$ time complexity, we will end up with a $\Tilde{O}(2^\frac n2 \, 2^{\frac n2(1-\epsilon)})$ algorithm. Solving the calculation we have
\begin{align*}
    2^\frac n2 \, 2^{\frac n2(1-\epsilon)}
    &= 2^{\frac n2 + \frac n2(1-\epsilon)}\\
    &= 2^{(1 + 1 - \epsilon)\frac n2}\\
    &= 2^{\frac{(2-\epsilon)}{2} n}
\end{align*}
This demonstrate that $A$ has $\Tilde{O}(2^{\frac{(2-\epsilon)}{2} n})$ time complexity. Since $\frac{(2-\epsilon)}{2} < 1$ we conclude that this implies to be able to solve \emph{SAT} in $O(2^{\alpha n})$ time with $\alpha < 1$, contradicting \emph{SETH}.

QED.

\section{Extensions}\label{section:extentions}
\subsection{DAGs}
The proof of Theorem \ref{theorem:Emlowerbound} can be easily modified in order to work also for DAGs.

\begin{corollary}\label{corollary:DAGs}
Given any labeled DAG $G'$, Theorem \ref{theorem:Emlowerbound} holds for $G'$.
\end{corollary}
What we mean by \emph{``Theorem \ref{theorem:Emlowerbound} holds for $G'$''} is that in defining \emph{PMLG} we replace the graph of Definition \ref{definition:labeledgraph} with a DAG.
\begin{proof}
Consider the definitions of edges $E_F$ and $E_U$ in the proof of Theorem \ref{theorem:Emlowerbound}. Using such definitions and considering the edges oriented is the only modification needed to make the proof of Theorem \ref{theorem:Emlowerbound} valid also for DAG $G'$.
\end{proof}
Intuitively, the reason why this corollary is so immediate resides in the fact that the labels that we are using in the undirected graph are already ``forcing'' the pattern to follow a specific direction.

\subsection{Limitations on the Degree}
We now take into account some specific situations showing that making stronger assumptions on the structure of the graph not always allows to solve \emph{PMLG} in less than $O(Em)$. In particular, we show that it is possible to modify the proof of Theorem \ref{theorem:Emlowerbound} in order to make it work for any undirected graph of degree at least $3$.

\begin{corollary} \label{corollary:degree3undirected}
Given a labeled undirected graph $G'$ of maximum degree $g$, if $g \ge 3$ then Theorem \ref{theorem:Emlowerbound} holds for $G'$.
\end{corollary}
\begin{proof}
Looking at the graph $G$ we used in the proof of Therorem \ref{theorem:Emlowerbound} we notices that it could have degree $2 \cdot 2^\frac n2 = 2^{\frac n2 + 1}$. If we could define a graph of degree $3$ which maintains the same properties we would have concluded the proof. To this end, first we modify gadgets $G_F$ and $G_U$ to meet such requirement, then we adjust pattern $P$ in order to be consistent with them. \paragraph{Revised $G_F$.}
Consider the at most $2^{\frac n2 + 1}$ edges connecting node $b$ with nodes $c_{j1}$ and $d_{j1}$ in $G_F$. We substitute such edges with a binary tree structure whose nodes are new dummy nodes $f_l$ with labels $L(f_l) = \texttt{f}$ for $1 \le l \le 2^{\frac n2} - 2$. Such \texttt{f} character is introduced only for the sake of exposition and later we will show how to avoid it. We replace the edges connecting nodes $c_{jk}$ and $d_{jk}$ with node $e$ with the same kind of tree. In this way, every inner node of such tree will be of degree at most $3$. This construction is depicted in Figure \ref{figure:Degree3GFtransf} \textit{(a)}. The $f$ nodes are the inner nodes of the tree but the root. Since the leaves are the $c_{j1}$ and $d_{j1}$ nodes, there are $2 \cdot 2^\frac n2 = 2^{\frac n2 + 1}$ of them. This means we have $2^\frac n2 - 1$ inner nodes while the $f$ nodes are $2^\frac n2 - 2$, since we the root is node $b$. This is why the $l$ index spanning the $f$ nodes is going up to $2^\frac n2 -2$. Adding this binary tree is not enough to achive degree $3$ in $G_F$ since nodes $c_{jh}$ and $d_{jh}$ could be of degree $4$. In fact, consider nodes $d_{jh-1}, d_{jh}$ and $d_{jh+1}$.  If nodes $c_{jh-1}, c_{jh}$ and $c_{jh+1}$ are all present, then both $c_{jh}$ and $d_{jh}$ both have degree $4$. As Figure \ref{figure:Degree3GFtransf} \textit{(b)} illustrates, we add two pairs of dummy nodes $f$ to lower the degree to $3$. One pair is placed between nodes $c_{jh-1}, d_{jh-1}$ and nodes $c_{jh}, d_{jh}$ via edges $(c_{jh-1},f_{jh-1}^\textit{(1)}),(d_{jh-1},f_{jh-1}^\textit{(1)})$ and $(f_{jh-1}^\textit{(2)},c_{jh}),(f_{jh-1}^\textit{(2)},d_{jh})$. The other pair of dummy nodes $f$ is placed between nodes $c_{jh}, d_{jh}$ and nodes $c_{jh+1}, d_{jh+1}$ via edges $(c_{jh},f_{jh}^\textit{(1)}),(d_{jh},f_{jh}^\textit{(1)})$ and $(f_{jh}^\textit{(2)},c_{jh+1}),(f_{jh}^\textit{(2)},d_{jh+1})$.\\
At the end of the entire transformation process for $G_F$ we are adding at most $2^\frac n2-2$ dummy nodes $f$ for the binary tree, and at most $O((k-1)2^\frac n2) = \Tilde{O}(2^\frac n2)$ pairs of nodes $f_{jh}^\textit{(1)}, f_{jh}^\textit{(2)}$ are placed. Moreover, the new edges for the binary tree are as many as the nodes while for the other modifications we add one edge for each pair of dummy nodes. Thus, the overall time complexity of the reduction does not change.

\begin{figure}[h]
\centering
\includegraphics[scale=0.40]{Figures/Degree3GFTransformation.pdf}
\caption{The transformation of gadget $G_F$. \textit{(a)} The edges connecting node $b$ with nodes $c_{j1}$ and $d_{j1}$ are replaced by a binary tree structure. Note that, even if not reported in this figure, the same process is performed for the edges connecting nodes $c_{jk}$ and $d_{jk}$ with node $e$. \textit{(b)} When $c_{jh-1}, c_{jh}$ and $c_{jh+1}$ are all present $c_{jh}$ and $d_{jh}$ have degree $4$ , hence pairs of dummy nodes are added to achieve degree $3$.}
\label{figure:Degree3GFtransf}
\end{figure}

\paragraph{Revised $G_U$.}
First of all, gadget $G_U$ has to be consistent with $G_F$. Due to this, we add $\log 2^{\frac n2 + 1} - 1 = \frac n2$ dummy nodes $f$ between every $b_i$ node of $G_U$ and the following $c_{i1}$ and $d_{i1}$ nodes. We also add $\frac n2$ dummy nodes $f$ between every node $e_i$ and the previous $c_ik$ and $d_ik$ nodes. We are adding $2\frac n2 (2^\frac n2-1) = \Tilde{O}(2^\frac n2)$ new nodes and one new edge per node, thus the overall time complexity will not be affected. The need for this step will be clearer when we will modify pattern $P$.\\
The other issue we have to handle is that in $G_U$, as in $G_F$, there are $c_{ih}$ and $d_{ih}$ nodes of degree $4$. Indeed, we add pairs of dummy nodes $f$ following the same schema presented for $G_F$ and illustrated in Figure \ref{figure:Degree3GFtransf} \textit{(b)}. Again, in this way we are introducing $(k-1)(2^\frac n2 -1) = \Tilde{O}(2^\frac n2)$ new nodes and one edge for each pair of dummy nodes which do not change the time complexity of the reduction.\\
Obviously, each dummy node placed in $G_U$ has label $\texttt{f}$.

\paragraph{Revised P.}
Consider patter $P = \texttt{b}P_{x_1}\texttt{eb}P_{x_2}\texttt{e} \ldots \texttt{b}P_{x_{2^{\frac{n}{2}}}}\texttt{e}$. In order to make it able to match the graph we need to add $\frac n2$ $\texttt{f}$ characters after every $\texttt{b}$ character and before every $\texttt{e}$ character. In this way, it will be possible to match both $G_F$ and $G_U$ thanks to the modifications we applied to them. Moreover, we have to insert $\texttt{f}$ characters also in the $P_{x_i}$ subpatterns. Given a subpattern $P_{x_i} = a_1 \, a_2 \ldots a_k$ where $a_h \in \{ \texttt{c,d}\}$, we insert the $\texttt{f}$ characters in order to obtain the new subpattern $P_{x_i}' = a_1 \texttt{f} \, \texttt{f} \, a_2 \, \texttt{f} \, \texttt{f} \ldots \texttt{f} \, \texttt{f} a_k$. Therefore, new pattern we will try to find a match for is:
\begin{equation*}
    P' = \texttt{b} \texttt{f}^{(1)} \ldots \texttt{f}^{(\frac n2)} P_{x_1}\texttt{f}^{(1)}  \ldots \texttt{f}^{(\frac n2)} \texttt{e} \ldots \texttt{b} \texttt{f}^{(1)} \ldots \texttt{f}^{(\frac n2)} P_{x_{2^{\frac{n}{2}}}}\texttt{f}^{(1)} \ldots \texttt{f}^{(\frac n2)}\texttt{e}
\end{equation*}
The number of new characters we are adding between and after the subpatterns is $\frac n2 2^\frac n2 = \Tilde{O}(2^\frac n2)$ while the ones inserted within the $P_{x_i}$ are $2(k-1) 2^\frac n2 = \Tilde{O}(2^\frac n2)$. Therefore there are no issues for what regarding the time complexity of the reduction.\\
\\
Therefore, we can conclude that Theorem \ref{theorem:Emlowerbound} holds for any specific case of an undirected graph having degree at least $3$.
\end{proof}

\begin{remark}
In this extension to the original proof we used an alphabet $\Sigma''$ of $5$ characters, namely $\Sigma'' = \{ \texttt{b,e,c,d,f} \}$. Looking at the structure of the graph and the pattern one can easily notice that character $f$ can be replaced with any other character without causing any problem. Hence, we can avoid introducing \texttt{f} using any one of the character in the alphabet $\Sigma = \{ \texttt{b,e,c,d} \}$ in place of it.
\end{remark}

We know explain how to extend the proof of Corollary \ref{corollary:degree3undirected} to the case of a DAG. In order to do so it has to be specified which are limitations for the edges of a single node in terms of indegree and outdegree. The corollary we present holds for any DAG $G'$ of indegree and outdegree at least $2$.
\begin{corollary}
Given a labeled DAG $G'$ of maximum indegree $\mathtt{in}$ and outdegree $\mathtt{out}$, if $\mathtt{in} \ge 2$ and $\mathtt{out} \ge 2$ then Theorem \ref{theorem:Emlowerbound} holds for $G'$.
\end{corollary}
\begin{proof}
Consider the proof of Theorem \ref{theorem:Emlowerbound}. Apply the same modifications as in the proof of Corollary \ref{corollary:degree3undirected} and then, as in Corollary \ref{corollary:DAGs}, consider the edges oriented. These modifications are enough to conclude the proof.
\end{proof}

The reason why we need both the indegree and the outdegree to be at least $2$ resides in the structure of the graph used for the reduction. In the revised version of $G_F$ presented in the proof of \ref{corollary:degree3undirected} nodes $b$ and $e$ are connected to the inner part of the graph by two tree structures. If we make the edges of such trees directed, the one connected with node $b$ could have outdegree $2$ while the one connected with node $e$ could have indegree $2$. The same situations arises in the pairs of $f$ nodes in the revised versions of both $G_F$ and $G_U$. One $f$ node will have indegree $2$ while the other will have outdegree $2$.

% \subsection{Extensions}
% \todo{rg}{aggiunto}
% Il risultato condizionale vale anche 
% \begin{itemize}
% \item se il max grado e' 3 (invece che 4)
% \item se il grafo e' DAG
% \end{itemize}

% Come ti ha detto Luca, nel caso che il max grado e' 2, ci sono due soli possibii casi, e sarebbe interessante capire la complessita' in tal caso, se si scende sotto $O(Em)$, cosi' completi il quadro.