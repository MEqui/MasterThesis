\documentclass{beamer}
%
% Choose how your presentation looks.
%
% For more themes, color themes and font themes, see:
% http://deic.uab.es/~iblanes/beamer_gallery/index_by_theme.html
%
\mode<presentation>
{
  \usetheme{default}      % or try Darmstadt, Madrid, Warsaw, ...
  \usecolortheme{default} % or try albatross, beaver, crane, ...
  \usefonttheme{default}  % or try serif, structurebold, ...
  \setbeamertemplate{navigation symbols}{}
  \setbeamertemplate{caption}[numbered]
} 

\usepackage[english]{babel}
\usepackage[utf8x]{inputenc}
\usepackage{pdfpages}
\usepackage{caption}


\title[Pattern Matching in Labeled Graphs]{Pattern Matching in Labeled Graphs}
\author{ Massimo Equi\\
    {
    \bigskip
    \small
    supervisor:  Roberto Grossi\\
    supervisor: Veli Mäkinen\\
    examiner: Gianna M. Del Corso
    }
}
\institute{Universit\`a di Pisa}
\date{05/10/2018}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}{Outline}
  \tableofcontents
\end{frame}

\section{The PMLG Problem}
\begin{frame}{Idea of the Problem}
    Pattern Matching in Labeled Graphs (\emph{PMLG})
    \only<1>{
        \begin{figure}
            \centering
            \includegraphics[scale=.70]{Figures/MatchExample_Presentation(1).pdf}
        \end{figure}
    }
    
    \only<2>{
        \begin{figure}
            \centering
            \includegraphics[scale=.70]{Figures/MatchExample_Presentation(2).pdf}
        \end{figure}
    }
\end{frame}

\begin{frame}{Formal Definitions}
    \begin{definition}
    $G = (V,E,L)$ is a \textbf{Labeled Graph} under an alphabet $\Sigma$ where:
    \begin{itemize}
        \item $(V,E)$ is a directed or undirected \textbf{graph}
        \item $L: V \rightarrow \Sigma^+$ is a \textbf{function} that defines which string is assigned to each node
    \end{itemize}
\end{definition}
\begin{definition}
    $u_1, \ldots, u_j$ is a \textbf{Match} for $P$ in $G$ with offset $l$ if:
    \begin{itemize}
        \item $u_1, \ldots, u_j$ ia a path in $(V,E)$
        \item $L(u)[l,*]$ is the suffix of $L(u)$ starting at position $l$
        \item $L(u)[*,l']$ is the prefix of $L(u)$ ending at position $l'$
        \item $L(u_1)[l,*] \cdot L(u_2) \cdot \ldots \cdot L(u_{j-1}) \cdot L(u_j)[*,l']$ matches $P$, for some $l'$
    \end{itemize}
\end{definition}
\end{frame}

\begin{frame}{Formal Definitions}
    The \emph{PMLG} problem is then defined as:
    \begin{itemize}
        \item[] \textbf{INPUT:} a labeled graph $G = (V,E,L)$ and a pattern $P$
        \item[] \textbf{OUTPUT:} all the matches for $P$ in $G$
    \end{itemize}
    
    \bigskip
    
    We will focus on the decision version of the problem, i.e.
    \begin{itemize}
        \item[] \textbf{INPUT:} a labeled graph $G = (V,E,L)$ and a pattern $P$
        \item[] \textbf{OUTPUT:} Yes/No - Determine whether there is at least one match for $P$ in $G$
    \end{itemize}
\end{frame}


\section{Motivations}
\begin{frame}{Motivations}
    \bigskip
    {\Large Biological Applications (Pangenomics)\\}
    \begin{figure}
        \centering
        \includegraphics[scale=.37]{Figures/ThachukTranscriptome.pdf}
        \caption*{Image taken from \cite{thachuk2012indexing}}
    \end{figure}
    \begin{itemize}
        \item $S$ = Splicing Graph, $G$ = Genome, $T$ = Transcriptome
        \item $t_i$ = Transcript $i$, $e_i$ = Exon $i$
    \end{itemize}
\end{frame}

\begin{frame}{Motivations}
    Theoretical interest for \emph{PMLG}
    \begin{table}[t]
    \centering
    \small
    \begin{tabular}{|c|c|c|c|c|}
         \hline
         \multicolumn{5}{|c|}{State of the art for \emph{PMLG}}\\
         \hline\hline
         \textbf{Year} & \textbf{Authors} & \textbf{Graph} & \textbf{Exact/} & \textbf{Time}\\
         ~ & ~ & ~ & \textbf{Approximate} & ~\\
         \hline
         1992 & Manber, Wu & DAG & approximate$^{\textit{(1)}}$ & $O(m|E| + occ\lg\lg m)$\\
         \hline
         1993 & Akutsu & Tree & exact\phantom{$^{\textit{(3)}}$} & $O(N)$\\
        \hline
        1995 & Park, Kim & DAG & exact$^{\textit{(3)}}$ & $O(N + m|E|)$\\
        \hline
        \textbf{1997} & \textbf{Amir et al.} & \textbf{general} & \textbf{exact\phantom{$^{\textit{(3)}}$}} & \boldmath$O(N + m|E|)$\\
        \hline
        1997 & Amir et al. & general & approximate$^{\textit{(2)}}$ & NP-Hard\\
        \hline
        1997 & Amir et al. & general & approximate$^{\textit{(1)}}$ & $O(Nm\lg N + m|E|)$\\
        \hline
        1998 & Navarro & general & approximate$^{\textit{(1)}}$ & $O(Nm + m|E|)$\\
        \hline
        2017 & Vadaddi et al. & general & approximate$^{\textit{(1)}}$ & $O((|V|+1)m|E|)$\\
        \hline
        \textbf{2017} & \textbf{Rautiainen,} & \textbf{general} & \textbf{approximate$^{\textit{(1)}}$} & \boldmath$O(N + m|E|)$\\
        ~ & \textbf{Marschall} & ~ & ~ & ~\\
        \hline
    \end{tabular}
    \caption{\textit{(1)} errors only in the pattern, \textit{(2)} errors in the graph, \textit{(3)} matches span only one edge. $N$ = total length of text in all nodes, $V$ = nodes, $E$ = edges, $m$ = length of the pattern, $occ$ = $\#$matches.}
    \end{table}
    
    % \begin{table}[h]
    %     \centering
    %     \small
    %     \begin{tabular}{|c|c|c|c|c|}
    %         \hline
    %         \textbf{Year} & \textbf{Authors} & \textbf{Graph} & \textbf{Exact/} & \textbf{Time}\\
    %         ~ & ~ & ~ & \textbf{Approx} & ~\\
    %         \hline
    %         1992 & Manber, Wu & DAG & approx$^{\textit{(1)}}$ & $O(V + Em +$\\
    %         ~ & ~ & ~ & ~ & $occ\log\log m)$\\
    %         \hline
    %         1993 & Akutsu & Tree & exact & $O(N)$\\
    %         \hline
    %         1995 & Park, Kim & DAG & exact$^{\textit{(3)}}$ & $O(N + Em)$\\
    %         \hline
    %         1997 & Amir et al. & general & exact & $O(N + Em)$\\
    %         \hline
    %         1997 & Amir et al. & general & approx$^{\textit{(1)}}$ & $O(Nm\log N + Em)$\\
    %         \hline
    %         1997 & Amir et al. & general & approx$^{\textit{(2)}}$ & NP-Hard\\
    %         \hline
    %         1998 & Navarro & general & approx$^{\textit{(1)}}$ & $O(Nm + Em)$\\
    %         \hline
    %         2017 & Vadaddi et al. & general & approx$^{\textit{(1)}}$ & $O((V+1)Em)$\\
    %         \hline
    %         2017 & Rautiainen, & general & approx$^{\textit{(1)}}$ & $O(N + Em)$\\
    %         ~ & Marschall & ~ & ~ & ~\\
    %         \hline
    %     \end{tabular}
    %     \caption{\textit{(1)} errors only in the pattern, \textit{(2)} errors in the graph, \textit{(3)} matches span only one edge. $N$ = total length of text in all nodes, $V$ = $\#$nodes, $E$ = $\#$edges, $m$ = length of the pattern, $occ$ = $\#$matches.}
    % \label{table:summary}
    % \end{table}
\end{frame}

% \begin{frame}{Motivations}
%     \Large Theoretical interest for \emph{PMLG}\\
%     \vfill
%     \large
%     State of the Art
%     \bigskip
%     \begin{itemize}
%         \item 1997: $O(N + Em)$ for \textbf{exact} matching, Amir et al. \cite{amir1997pattern}
%         \item 2017: $O(N + Em)$ for \textbf{approximate} matching, Rautiainen and Marchall \cite{rautiainen2017aligning}
%     \end{itemize}
% \end{frame}
% No lower bound for the problem yet
% Best algorithms for DAGs, exact and approximate had the same time complexity
% Table with the State of the Art

\section{Goal}
\begin{frame}{Goal}
    Notation: $E =$ number of edges, $m =$ length of the pattern\\
    \bigskip
    Approximate matching:
    \begin{itemize}
        \item No algorithm can solve \textbf{approximate} \emph{PMLG} in either $O(E^{1-\epsilon}m)$ or $O(Em^{1-\epsilon}), \; \epsilon > 0$ unless \emph{SETH} is false \cite{backurs2015edit}
        \item Such result for \textbf{approximate} \emph{PMLG} can be easily derived from the \textbf{string matching}  case \cite{rautiainen2017aligning}
    \end{itemize}
    
    \onslide<2->{
    \bigskip
    Exact matching
    \begin{itemize}
        \item No conditional lower bound for the \textbf{exact} case yet (our goal)
        \item Such result for \textbf{exact} \emph{PMLG} \textbf{can not} be derived from the \textbf{string matching}  case (it's linear time)
    \end{itemize}
    }
    % \bigskip
    % Our goal:\\
    % Find a \textbf{Conditional Lower Bound} for \emph{PMLG} in the case of \textbf{exact} matching
    
\end{frame}

\begin{frame}{Goal}
    Notation: $E =$ number of edges, $m =$ length of the pattern\\
    \bigskip
    Find a \textbf{Conditional Lower Bound} for \emph{PMLG}\\
    \bigskip
    What we will prove:
    \begin{itemize}
        \item No algorithm can solve exact \emph{PMLG} in either $O(E^{1-\epsilon}m)$ or $O(Em^{1-\epsilon}), \; \epsilon > 0$ unless \emph{SETH} fails
        % \item The lower bound holds if \emph{SETH} is true
    \end{itemize}
    
    \onslide<2->{
    \bigskip
        \emph{SETH}: Strong Exponential Time Hypothesis
        \begin{itemize}
            \item based on the complexity of \emph{SAT} \cite{impagliazzo1999complexity}
            \item used for proving several conditional lower bounds \cite{vassilevska2018fine-grained}
        \end{itemize}
    }
    
\end{frame}
% Prove a Conditional Lower Bound
\section{Theorem}
\begin{frame}{Definition of SETH}
    Strong Exponential Time Hypothesis (\emph{SETH})
    \begin{definition}
        Let \emph{qSAT} be an instance of \emph{SAT} with at most \emph{q} literals per clause.\\
        $n =$ number of boolean variables.\\
        Given
        \begin{equation*}
        \delta_q = inf \, \{\alpha \, : \, \text{There is an} \, O(2^{\alpha n}) \, \text{ time algorithm for qSAT}) \}
        \end{equation*}
        we state the following
        \begin{equation*}
            \text{\textbf{SETH:}} \qquad \lim\limits_{q \to \infty} \delta_q = 1
        \end{equation*}
    \end{definition}
    Hence no $O(2^{\alpha n})$ time and $\alpha < 1$ algorithm for \emph{SAT} is allowed if \emph{SETH} is true
\end{frame}

\begin{frame}{Conditional Lower Bound}
    \Large
    Find a \textbf{conditional lower bound} for the\\
    \emph{Pattern Matching in Labeled Graphs} problem\\
    \bigskip
    \begin{theorem} For any $\epsilon > 0$ and any alphabet $\Sigma$ such that $|\Sigma| \ge 4$, the \emph{Pattern Matching in Labeled Graphs} problem can not be solved in either $O(E^{1-\epsilon} \, m)$ or $O(E \, m^{1-\epsilon})$ time unless \emph{SETH} is false. \end{theorem}
\end{frame}

\begin{frame}{Implications}
    \Large
    Such theorem shows that:
    \begin{itemize}
        \item the 1997 algorithm \cite{amir2000pattern} for \textbf{exact} \emph{PMLG} is \textbf{optimal} under \emph{SETH} 
        \item the cost for \textbf{exact} \emph{PMLG} and \textbf{approximate} \emph{PMLG} is the same under \emph{SETH}, which is not the case for pattern matching on strings (linear vs quadratic)
    \end{itemize}
\end{frame}

\begin{frame}{Proof}
    \begin{theorem} For any $\epsilon > 0$ and any alphabet $\Sigma$ such that $|\Sigma| \ge 4$, the \emph{Pattern Matching in Labeled Graphs} problem can not be solved in either $O(E^{1-\epsilon} \, m)$ or $O(E \, m^{1-\epsilon})$ time unless \emph{SETH} is false. \end{theorem}
    \bigskip
    {\large \textbf{Proof}\\}
    \only<1>{
        \bigskip
        Find a \textbf{reduction} from \emph{SAT} to \emph{PMLG};\\
        \bigskip
        Show a \textbf{contradiction} with \emph{SETH}.\\
    }
    \onslide<2->{
        Reduction
        \begin{itemize}
            \item \emph{SAT} formula $F$
            \item Pattern $P$ of length $m = \tilde{O}(2^\frac n2)$
            \item Labeled Graph $G$ such that $E = \tilde{O}(2^\frac n2)$
            \item $P$ has a match in $G \iff F$ is satisfiable
        \end{itemize}
    }
    \onslide<3>{
        Contradiction
        \begin{itemize}
            \item The cost of building $P$ and $G$ has to be $O(2^{\alpha n}), \alpha < 1$
            \item Show that a $O(E^{1-\epsilon}m)$ or $O(Em^{1-\epsilon})$ algorithm for \emph{PMLG} contradicts \emph{SETH}
        \end{itemize}
    }
    % Using such a reduction and assuming to have a $O(E^{1-\epsilon}m)$ or $O(Em^{1-\epsilon})$ algorithm for \emph{PMLG} we can prove to have a contradiction with \emph{SETH}.
 \end{frame}
 
\begin{frame}{Pattern}
Alphabet: $\Sigma = \{ \texttt{b},\texttt{e},\texttt{c},\texttt{d} \}$\\
Variables: $v_1, \ldots, v_n$\\
Sets of truth assignments:
\begin{align*}
X =& \; \{x_i \, | \; x_i \quad \text{is a possible truth assignment for} \quad v_1, \ldots,  v_{\frac{n}{2}} \}&\\
Y =& \; \{y_j \, | \; y_j \quad \text{is a possible truth assignment for} \quad v_{\frac{n}{2}+1}, \ldots,  v_n \}&\\
&|X| = |Y| = 2^\frac n2
\end{align*}

Pattern: $P = \texttt{b}P_{x_1}\texttt{eb}P_{x_2}\texttt{e} \ldots \texttt{b}P_{x_{2^{\frac{n}{2}}}}\texttt{e}$
    \begin{equation*}
P_{x_i}[h] =
    \begin{cases}
        \texttt{c} \qquad  \text{if} \quad x_i \not \models c_h\\
        \texttt{d} \qquad  \text{otherwise} 
    \end{cases}
\end{equation*}
\end{frame}

\begin{frame}{Pattern}
    $v_1, v_2, v_3, v_4$ variables; \quad $c_1, c_2, c_3$ clauses
    \large
    \begin{align*}
    &
    \begin{matrix}
        ~ & c_1 & ~ & c_2 & ~ & c_3\\
        F = &(v_1 \vee v_4)& \wedge &(\neg v_1 \vee v_2 \vee v_3)& \wedge &(\neg v_2 \vee \neg v_4)\\
    \end{matrix}&\\
    \\
    &
    \begin{matrix}
        ~ & P_{x_1} & P_{x_2} & P_{x_3} & P_{x_4}\\
        P = &\texttt{b cdd e} & \texttt{b cdc e} & \texttt{b dcd e} & \texttt{b ddc e}
    \end{matrix}&
    \end{align*}
    
    \begin{figure}
        \centering
        \includegraphics[scale=.40]{Figures/TruthAssignment_Presentation.pdf}
    \end{figure}
\end{frame}

\begin{frame}{Gadget $G_F$}
    Node $c_{j h}$ exists $\iff y_j \models c_h$\\
    \only<1>{
    \begin{equation*}
    \begin{matrix}
        ~ & c_1 & ~ & c_2 & ~ & c_3\\
        F = &(v_1 \vee v_4)& \wedge &(\neg v_1 \vee v_2 \vee v_3)& \wedge &(\neg v_2 \vee \neg v_4)\\
    \end{matrix}
    \end{equation*}
    \begin{figure}
        \centering
        \includegraphics[scale=.30]{Figures/G_Fgadget_Presentation_Alternative.pdf}
    \end{figure}
   }
    \only<2>{
    \begin{align*}
        P_{x_1} =& \texttt{b cdd e} \quad\text{ has a match}\\
        P_{x_2} =& \texttt{b cdc e} \quad\text{ has not}
    \end{align*}
    \begin{figure}
        \centering
        \includegraphics[scale=.30]{Figures/G_Fgadget_Presentation_Match.pdf}
    \end{figure}
    }
    
\end{frame}

\begin{frame}{Contradiction with \emph{SETH}}
A $O(2^{\alpha n}), \alpha < 1$ algorithm for \emph{SAT} would contradict \emph{SETH}
\begin{itemize}
    \item Assume to have a $O(E^{1-\epsilon}m)$ or $O(Em^{1-\epsilon})$ algorithm for \emph{PMLG}
    \item In our reduction $E = \tilde{O}(2^\frac n2), m = \tilde{O}(2^\frac n2)$
    \item The cost of the reduction is $\tilde{O}(2^\frac n2)$
\end{itemize}
\bigskip
Hence the time complexity is $\tilde{O}(2^\frac n2 \, 2^{\frac n2(1-\epsilon)})$. Therefore:

\begin{align*}
    2^\frac n2 \, 2^{\frac n2(1-\epsilon)}
    &= 2^{\frac n2 + \frac n2(1-\epsilon)}\\
    &= 2^{(1 + 1 - \epsilon)\frac n2}\\
    &= 2^{\frac{(2-\epsilon)}{2} n} = 2^{\alpha n} \quad \alpha<1
\end{align*}
This combined with our reduction contradicts \emph{SETH}.
\end{frame}

\section{Extensions}
\begin{frame}{Extensions}
\Large
This conditional lower bound holds even if:
\begin{itemize}
    \item the graph is undirected and has maximum \textbf{degree} $3$
    \item the graph is a \textbf{DAG} with maximum sum of \textbf{indegree} and \textbf{outdegree} $3$
\end{itemize}
\end{frame}

\section{Conclusions and Future Works}
\begin{frame}{Conclusions}
\Large
    The achieved results:
    \begin{itemize}
        \item Better awareness of the complexity of the problem
        \item State-of-the-art algorithms are hard to improve (unless \emph{SETH} is false)
        \item In case of graph preprocessing, the lower bound holds for the time needed to build the index
    \end{itemize}
    \bigskip
\end{frame}

\begin{frame}{Future Works}
    \Large
    Alphabet Size\\
    \begin{description}
        \item from $|\Sigma| = 4$ to $|\Sigma| = 2$
    \end{description}
    \vfill
    Undirected graph of degree $2$: Zig-zag matching
    \begin{figure}
        \centering
        \includegraphics[scale=.40]{Figures/Degree2Undirected_Presentation.pdf}
    \end{figure}
    $O(Em)$ or $O(E + m)$?
\end{frame}

\bibliographystyle{alpha}
\begin{frame}[allowframebreaks]{References}
    \small
    \bibliography{Bibliography}
\end{frame}

\begin{frame}
\end{frame}

\begin{frame}{Graph}
    \Large
    Graph $G$ consists in two substructures:
    \bigskip
    \begin{itemize}
        \item $G_U = $ Universal Gadget, matches \textbf{any subpattern} $P_{x_i}$
        \item $G_F = $ Formula Gadget, matches a subpattern $P_{x_i}$ only \textbf{under certain conditions} on the \emph{SAT} formula $F$
    \end{itemize}
\end{frame}

\begin{frame}{Gadget $G_U$}
    \Large
    $n =$ number of variables, $k =$ number of clauses
    \bigskip
    {\LARGE
    $P = \texttt{b cdc eb ddc eb dcd e}$}
    \begin{figure}
        \centering
        \includegraphics[scale=.40]{Figures/G_Ugadget_Presentation.pdf}
    \end{figure}
\end{frame}

\begin{frame}{Big Picture}
    \Large
    $n =$ number of variables, $k =$ number of clauses
    \begin{figure}
        \centering
        \includegraphics[scale=.40]{Figures/CompleteGraph_Presentation.pdf}
    \end{figure}
    $P$ has a match in $G \iff F$ is satisfiable
\end{frame}

\begin{frame}{Extensions}
The reduction works for DAGs
    \only<1>{
        \begin{figure}
            \centering
            \includegraphics[scale=.35]{Figures/Extention_DAGs_Presentation(1).pdf}
        \end{figure}
    }
    \only<2>{
        \begin{figure}
            \centering
            \includegraphics[scale=.35]{Figures/Extention_DAGs_Presentation(2).pdf}
        \end{figure}
    }
The labels ``force'' a direction
\end{frame}

\begin{frame}{Extensions}
The reduction works for undirected graphs of degree $\le 3$.\\
    \only<1>{
        \begin{figure}
            \centering
            \includegraphics[scale=.40]{Figures/Degree3_Transf_Presentation(1).pdf}
        \end{figure}
    }
    \only<2>{
        \begin{figure}
            \centering
            \includegraphics[scale=.40]{Figures/Degree3_Transf_Presentation(2).pdf}
        \end{figure}
        To modify the pattern just add \texttt{f} characters according to the structure of the new graph
    }
\end{frame}

\end{document}
