# Pattern Matching in Hypertex

This is my master thesis work about the problem of finding a string that can
spread among several nodes of a directed graph.
The problem has been studied for 26 years and yet it is quite open.
Here are some of the hottest issues:
- What's the best online algorithm for finding an exact match?
- How well can we do on general graphs with an indexed approach?
- How well can we do choosing different definitions of distance for an approximate match?

All of these and many others are developing directions that are being motivated
by biology. Due to the rising of the NGS methods the study of the genome has experienced a
drastic growth during the last years. Thus new algorithmic theories and tools has
to be provided in order to meet these requests.
